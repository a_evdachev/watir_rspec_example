# spec/spec_authorization/validation_fields_spec.rb
require_relative '../../driver.rb'
require_relative './pojo_authorization/pogo_authorization.rb'

describe "Тестирование валидации полей на форме авторизации" do
  @browser, @pojo = nil

  before(:all) do
    driver = DriverChrome.new 

    @browser = driver.get_browser()
    @browser.goto URI("revealbot.com/signup")

    @pojo = PojoAithorization.new
  end

  after(:all) do
    @browser.close
  end

  context "Проверка валидации значения в поле Email" do 

    it "При вводе () получаем (Please enter a valid email address)" do 
      error = actions_on_fields('user_email', '', @pojo.err_email)
      expect('Please enter a valid email address.').to include(error)
    end

    it "При вводе (alex) получаем (Please enter a valid email address)" do 
      error = actions_on_fields('user_email', 'alex', @pojo.err_email)
      expect('Please enter a valid email address.').to include(error)
    end

    it "При вводе (alex@alex) получаем (Please enter a valid email address)" do 
      error = actions_on_fields('user_email', 'alex@alex', @pojo.err_email)
      expect('Please enter a valid email address.').to include(error)
    end

    it "При вводе (alex@alex.com) получаем ()" do 
      error = actions_on_fields('user_email', 'alex@alex.com', @pojo.err_email)
      expect('').to include(error)
    end
  end

  private

  def actions_on_fields(field, value_fild, error_field)
    text_field = @browser.text_field id: field
    text_field.exists?
    text_field.set value_fild

    @browser.element(xpath: @pojo.cb_agreement).click # checkbox скрыт элементом

    button = @browser.button value: 'Create Account'
    button.exists?
    button.click
 
    span = @browser.span xpath: error_field
    if span.exists?
      span.text        
    else
      return ''
    end
  end
end