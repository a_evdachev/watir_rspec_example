# https://revealbot.com/signup

class PojoAithorization
  attr_reader :cb_agreement, :inp_email, :but_create, :err_email
  
  def initialize()
    @cb_agreement = '//*[@id="new_user"]/div[5]/div/div/label' 
    @inp_email = '//*[@id="user_email"]'
    @but_create = '//*[@id="new_user"]/div[6]/div/input'
    @err_email = '//*[@id="new_user"]/div[3]/div/div/span'
  end
end