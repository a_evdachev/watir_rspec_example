require 'watir'
require 'uri'

class DriverChrome
  Selenium::WebDriver::Chrome.driver_path = "./chromedriver" # Задаем муть к драйверу

  def get_browser()
    Watir::Browser.new :chrome
  end 
end